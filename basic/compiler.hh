#pragma once

#define NINROB_CALL_BEFORE_MAIN __attribute__((constructor))
#define NINROB_CALL_BEFORE_MAIN_WITH_PRIORITY(prio) \
  __attribute__((constructor(prio)))

#define NINROB_SURE(expr) __builtin_expect(static_cast<bool>(expr), 1)
#define NINROB_DOUBT(expr) __builtin_expect(static_cast<bool>(expr), 0)

#define NINROB_COMPILE_COLD_CODE __attribute__((cold))

#define NINROB_NO_RETURN [[noreturn]]

#define NINROB_ARTIFICIAL_FUNCTION __attribute__((artificial))
