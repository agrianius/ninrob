#include "trace.hh"
#include <vector>

namespace ninrob::trace {


struct SchemeRegistryItem {
  TracePointId scheme_id = 0;
  FormatMessageId format_message_id = 0;
  std::vector<FieldId> field_ids;
  std::vector<u4> offsets;
  std::vector<u3> field_type_ids;
};


std::vector<SchemeGenerator>& GetProcessWideSchemeGeneratorsRegistry() noexcept;
std::vector<SchemeRegistryItem>& GetInternalSchemeRegistry() noexcept;


struct Formatter {
  const SchemeRegistryItem* scheme;
  const char* raw_msg;
  size_t raw_msg_size;
  const char* data;
  char* buf;
  size_t buf_size;

  static constexpr size_t MIN_BUFFER_SIZE = 16;

  void FormatMessage() noexcept;
  virtual void GetNextBuffer() noexcept = 0;

private:
  void GetNextBufferAndVerify() noexcept;
  void CopyFromRawToBuf(u4 size_to_copy) noexcept;
  void CopyFromNullStrToBuf(const void* data) noexcept;
  template <class T>
  void BufferredFormatFieldOrDie(const void* data) noexcept;
  void ChooseFormatFieldOrDie(u3 field_type_id, const void* data) noexcept;
};


}
