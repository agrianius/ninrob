#include "trace.hh"
#include "trace_impl.hh"
#include "verify.hh"

#include <algorithm>
#include <charconv>
#include <cstring>
#include <iostream>
#include <vector>

namespace ninrob::trace {


std::vector< SchemeGenerator>&
GetProcessWideSchemeGeneratorsRegistry() noexcept {
  static std::vector< SchemeGenerator> registry;
  return registry;
}


void AddSchemeGeneratorToProcessWideRegistry( SchemeGenerator gen) noexcept {
  auto& registry = GetProcessWideSchemeGeneratorsRegistry();
  registry.push_back( gen);
}


std::atomic< u5> process_wide_tracepoint_id_source{0};
std::atomic<TracePointId> process_wide_scheme_id_source{0};
std::atomic< Publisher*> process_wide_publisher{nullptr};



std::vector< SchemeRegistryItem>& GetInternalSchemeRegistry() noexcept {
  static std::vector< SchemeRegistryItem> registry;
  return registry;
}


NINROB_COMPILE_COLD_CODE
void InsertIntoRegistry( SchemeRegistryItem item) noexcept {
  auto& registry = GetInternalSchemeRegistry();
  auto comparator =
    [](TracePointId val, const SchemeRegistryItem& item) noexcept {
      return val < item.scheme_id;
    };
  auto pos = std::upper_bound(
      registry.cbegin(), registry.cend(), item.scheme_id, comparator);
  registry.insert( pos, std::move(item));
  registry.shrink_to_fit();
}


NINROB_COMPILE_COLD_CODE
static void InternalSchemeRegistryScanner() noexcept {
  auto& gens_registry = GetProcessWideSchemeGeneratorsRegistry();
  gens_registry.shrink_to_fit();
  auto& registry = GetInternalSchemeRegistry();
  registry.reserve( gens_registry.size());
  for (auto gen: gens_registry) {
    struct Item
      : public SchemeRegistryItem
      , public FieldIdContainer
    {
      void FillIds( const RegistrationParameters& params) noexcept override {
        scheme_id = params.scheme_id;
        format_message_id = params.format_message_id;
        field_ids.assign( params.begin_ids, params.end_ids);
        offsets.assign( params.begin_offsets, params.end_offsets);
        field_type_ids.assign(
            params.begin_field_type_ids, params.end_field_type_ids);
        field_ids.shrink_to_fit();
        offsets.shrink_to_fit();
      }
    };
    Item item;
    gen( &item);
    registry.push_back( item);
  }
  auto comparator =
    [](const SchemeRegistryItem left, const SchemeRegistryItem right) noexcept {
      return left.scheme_id < right.scheme_id;
    };
  std::sort( registry.begin(), registry.end(), comparator);
}


void Formatter::GetNextBufferAndVerify() noexcept {
  GetNextBuffer();
  VerifyF( buf_size >= MIN_BUFFER_SIZE, "buffer is too small: %u", buf_size);
  VerifyF( buf + buf_size > buf, "buffer overlap");
}


void Formatter::CopyFromRawToBuf(u4 size_to_copy) noexcept {
  for (;;) {
    // it is safe to convert from size_t to u4 because result can't be greater
    // than size_to_copy, which is u4 type
    u4 chunk_to_copy =
      static_cast< u4>( std::min<size_t>( buf_size, size_to_copy));
    std::memcpy( buf, raw_msg, chunk_to_copy);
    raw_msg += chunk_to_copy;
    buf += chunk_to_copy;
    buf_size -= chunk_to_copy;
    size_to_copy -= chunk_to_copy;
    if (NINROB_SURE( size_to_copy == 0)) {
      return;
    }
    if (NINROB_DOUBT( buf_size == 0)) {
      GetNextBufferAndVerify();
    }
  }
}


void Formatter::CopyFromNullStrToBuf(const void* data) noexcept {
  VerifyF(data, "source string must not be nullptr");
  const char* src_str = *reinterpret_cast<const char* const *>(data);
  for (; *src_str != '\0'; ++src_str) {
    *buf = *src_str;
    if (NINROB_DOUBT( --buf_size == 0)) {
      GetNextBufferAndVerify();
    } else {
      ++buf;
    }
  }
}


// TODO: use std::to_chars
template <class T>
u3 FormatFieldOrDie(T val, char* dest_begin, u3 buffer_size) noexcept {
  if constexpr (std::is_same_v< T, double>) {
    const int result = snprintf( dest_begin, buffer_size, "%lf", val);
    VerifyF( result > 0, "error while formatting: %lf", val);
    return static_cast<u3>(result);
  } else if constexpr (std::is_same_v< T, float>) {
    const int result = snprintf( dest_begin, buffer_size, "%f", val);
    VerifyF( result > 0, "error while formatting: %f", val);
    return static_cast<u3>(result);
  } else if constexpr (std::is_same_v< T, u6>) {
    const int result = snprintf( dest_begin, buffer_size, "%lu", val);
    VerifyF( result > 0, "error while formatting: %lu", val);
    return static_cast<u3>(result);
  } else if constexpr (std::is_same_v< T, s6>) {
    const int result = snprintf( dest_begin, buffer_size, "%ld", val);
    VerifyF( result > 0, "error while formatting: %ld", val);
    return static_cast<u3>(result);
  } else if constexpr (std::is_integral_v< T> && std::is_unsigned_v< T>) {
    const int result = snprintf( dest_begin, buffer_size, "%u", val);
    VerifyF( result > 0, "error while formatting: %u", val);
    return static_cast<u3>(result);
  } else if constexpr (std::is_integral_v< T> && std::is_signed_v< T>) {
    const int result = snprintf( dest_begin, buffer_size, "%d", val);
    VerifyF( result > 0, "error while formatting: %d", val);
    return static_cast<u3>(result);
  } else {
    static_assert(std::is_same_v< T, float>, "unknown type");
  }
}


template <class T>
void Formatter::BufferredFormatFieldOrDie(const void* data) noexcept {
  const char* restore_raw = raw_msg;
  size_t restore_raw_size = raw_msg_size;

  constexpr u3 lbuf_size = FieldTypeEnumChoice< T>::required_buffer_size;
  char lbuf[ lbuf_size];
  raw_msg = std::begin(lbuf);
  raw_msg_size = FormatFieldOrDie<T>(
      *reinterpret_cast<const T*>(data), std::begin(lbuf), lbuf_size);
  CopyFromRawToBuf(raw_msg_size);

  raw_msg = restore_raw;
  raw_msg_size = restore_raw_size;
}


void Formatter::ChooseFormatFieldOrDie(
    u3 field_type_id, const void* data) noexcept
{
  using FormatterType = void (Formatter::*)( const void* data) noexcept;
  static const FormatterType
    formatter_func[ static_cast<u3>( FieldTypeEnum::MAX)] = {
      &Formatter::CopyFromNullStrToBuf,
      &Formatter::BufferredFormatFieldOrDie<u3>,
      &Formatter::BufferredFormatFieldOrDie<s3>,
      &Formatter::BufferredFormatFieldOrDie<u4>,
      &Formatter::BufferredFormatFieldOrDie<s4>,
      &Formatter::BufferredFormatFieldOrDie<u5>,
      &Formatter::BufferredFormatFieldOrDie<s5>,
      &Formatter::BufferredFormatFieldOrDie<u6>,
      &Formatter::BufferredFormatFieldOrDie<s6>,
      &Formatter::BufferredFormatFieldOrDie<float>,
      &Formatter::BufferredFormatFieldOrDie<double>,
  };
  return (this->*(formatter_func[ field_type_id]))( data);
}


void Formatter::FormatMessage() noexcept {
  constexpr u3 SMALLEST_STRING_WITH_BRACKETS = 4;
  VerifyF( buf_size >= MIN_BUFFER_SIZE, "buffer is too small: %u", buf_size);
  VerifyF( buf + buf_size > buf, "buffer overlap");
  if (NINROB_DOUBT( raw_msg_size < SMALLEST_STRING_WITH_BRACKETS)) {
    std::memcpy( buf, raw_msg, raw_msg_size);
    return;
  }

  VerifyF(raw_msg_size <= MAX_FORMAT_MESSAGE_SIZE,
          "message size is %lu but must be less than %u",
          raw_msg_size, MAX_FORMAT_MESSAGE_SIZE);
  static constexpr char open_brackets[2] = {'{', '{'};
  static constexpr char close_brackets[2] = {'}', '}'};
  const char* end_iter = raw_msg + raw_msg_size;
  VerifyF( end_iter > raw_msg, "buffer overlap");

  do {
    const char* open_found = std::search(
        raw_msg,
        end_iter,
        std::begin( open_brackets),
        std::end( open_brackets));
    if (NINROB_DOUBT( open_found == end_iter)) {
      break;
    }
    const char* close_found = std::search(
        open_found + 2,
        end_iter,
        std::begin( close_brackets),
        std::end( close_brackets));
    if (NINROB_DOUBT( close_found == end_iter)) {
      break;
    }
    // forward raw chars
    // it is safe to cast to unsigned, no chance for negative values
    CopyFromRawToBuf( static_cast<u4>( std::distance( raw_msg, open_found)));
    open_found += 2;
    if (NINROB_DOUBT( buf_size == 0)) {
      GetNextBufferAndVerify();
    }
    raw_msg = close_found + 2;
    if (NINROB_DOUBT( open_found == close_found)) {
      // nothing to do with empty format
      continue;
    }

    FieldId field_id;
    auto fc_result = std::from_chars( open_found, close_found, field_id);
    VerifyF(fc_result.ptr == close_found, "invalid field id");
    VerifyF(fc_result.ec == std::errc{}, "invalid field id");
    static_assert( std::is_unsigned_v< FieldId>);
    // there are at most MAX_NUMBER_OF_FIELDS fields
    // that's why the following cast is safe
    u3 const field_num = static_cast<u3>( std::distance(
        scheme->field_ids.cbegin(),
        std::find(
            scheme->field_ids.cbegin(), scheme->field_ids.cend(), field_id)));
    ChooseFormatFieldOrDie(
        scheme->field_type_ids[field_num], data + scheme->offsets[field_num]);
  } while (std::distance( raw_msg, end_iter) >= 4);

  CopyFromRawToBuf( static_cast<u4>( std::distance( raw_msg, end_iter)));
}


struct DefaultLogger: public Publisher {
  void PublishLogRecord( InternalLogRecord rec) noexcept override {
    struct FormatterWithBuf: public Formatter {
      static constexpr u4 reusable_buf_size() { return 512; };
      std::unique_ptr<char[]> reusable_buf{new char[reusable_buf_size()]};

      explicit FormatterWithBuf(InternalLogRecord rec) noexcept {
        buf = reusable_buf.get();
        buf_size = reusable_buf_size();
        data = rec.data;
        raw_msg = rec.format;
        raw_msg_size = std::strlen(rec.format);

        const auto& scheme_registry = GetInternalSchemeRegistry();
        auto comparator =
          [](const SchemeRegistryItem& item, TracePointId val) noexcept {
            return item.scheme_id < val;
          };
        auto found_it = std::lower_bound(
            scheme_registry.cbegin(),
            scheme_registry.cend(),
            rec.scheme_id,
            comparator);
        VerifyF(found_it != scheme_registry.cend(),
                "unknown trace point scheme id: %u", rec.scheme_id);
        scheme = &*found_it;
      }

      void GetNextBuffer() noexcept override {
        std::cerr << std::string_view(reusable_buf.get(), reusable_buf_size());
      }

      void Finish() noexcept {
        u4 filled_size =
          static_cast<u4>(std::distance(reusable_buf.get(), buf));
        std::cerr << std::string_view(reusable_buf.get(), filled_size)
                  << std::endl;
      }
    };

    FormatterWithBuf formatter{rec};
    formatter.FormatMessage();
    formatter.Finish();
  }

  NINROB_CALL_BEFORE_MAIN_WITH_PRIORITY(103)
  NINROB_COMPILE_COLD_CODE
  static void SelfSetup() noexcept {
    static DefaultLogger default_logger;
    InternalSchemeRegistryScanner();
    process_wide_publisher.store( &default_logger, std::memory_order_relaxed);
  }
};


}
