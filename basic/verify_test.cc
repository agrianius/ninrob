#include "verify.hh"
#include <gtest/gtest.h>
#include <memory>

TEST( Verify, Simple) {
  auto death_lambda =
    []() noexcept {
      ninrob::VerifyF(0, "should I fail?");
    };
  EXPECT_DEATH(death_lambda(), "should I fail?");
}


TEST( Verify, unique_ptr) {
  auto obj_ptr = ninrob::VerifyF(::std::make_unique<int>(42));
  ASSERT_TRUE(static_cast<bool>(obj_ptr));
  EXPECT_EQ(*obj_ptr, 42);
}


TEST( Verify, unique_ptr_ref) {
  auto obj_ptr = ::std::make_unique<int>(42);
  auto& ref_to_obj_ptr = ninrob::VerifyF(obj_ptr);
  EXPECT_EQ(&obj_ptr, &ref_to_obj_ptr);
  ASSERT_TRUE(static_cast<bool>(obj_ptr));
  EXPECT_EQ(*obj_ptr, 42);
}
