#pragma once

// short synonims for integer types

using u3 = unsigned char;
static_assert(sizeof(u3) == 1U);

using s3 = signed char;
static_assert(sizeof(s3) == 1U);

using u4 = unsigned short;
static_assert(sizeof(u4) == 2U);

using s4 = signed short;
static_assert(sizeof(s4) == 2U);

using u5 = unsigned int;
static_assert(sizeof(u5) == 4U);

using s5 = signed int;
static_assert(sizeof(s5) == 4U);

using u6 = unsigned long int;
static_assert(sizeof(u6) == 8U);

using s6 = signed long int;
static_assert(sizeof(s6) == 8U);
