#include "trace_impl.hh"
#include "log_severity.hh"
#include "gtest/gtest.h"


using ::ninrob::trace::NamedTupleConstructor;


TEST( Trace, Simple) {
  NamedTupleConstructor con0;
  auto con1 = con0.AddField<142, int>(42);
  auto con2 = con1.AddField<14, int>(43);
  auto con3 = con2.AddField<7, const char*>("yes");
  EXPECT_EQ(sizeof(con2.payload), 8U);
  EXPECT_EQ(sizeof(con2), 8U);
  EXPECT_EQ(con2.payload.val, 43);
  EXPECT_EQ(con2.payload.before.val, 42);
  EXPECT_EQ((con2.Get<0>()), 42);
  EXPECT_EQ((con2.Get<1>()), 43);
  EXPECT_EQ((con2.GetOffset<0>()), 0);
  EXPECT_EQ((con2.GetOffset<1>()), 4);
  auto ids = con2.GetIds();
  ASSERT_EQ(ids.size(), 2U);
  EXPECT_EQ(ids[0], 142U);
  EXPECT_EQ(ids[1], 14U);
  static ::ninrob::trace::TracePointId tp =
    ::ninrob::trace::GetNextTracePointId();
  con3.Publish(tp, "love me or {{142}} leave {{14}} me: {{7}}/no");
}


TEST( Trace, MAX_NUMBER_OF_FIELDS) {
  NamedTupleConstructor con;
  auto res = con
    .AddField<0, int>(42)
    .AddField<0, int>(42)
    .AddField<0, int>(42)
    .AddField<0, int>(42)
    .AddField<0, int>(42)
    .AddField<0, int>(42)
    .AddField<0, int>(42)
    .AddField<0, int>(42)
    .AddField<0, int>(42)
    .AddField<0, int>(42)
    .AddField<0, int>(42)
    .AddField<0, int>(42)
    .AddField<0, int>(42)
    .AddField<0, int>(42)
    .AddField<0, int>(42)
    .AddField<0, int>(42)
    .AddField<0, int>(42)
    .AddField<0, int>(42)
    .AddField<0, int>(42)
    .AddField<0, int>(42)
    .AddField<0, int>(42)
    .AddField<0, int>(42)
    .AddField<0, int>(42)
    .AddField<0, int>(42)
    .AddField<0, int>(42)
    .AddField<0, int>(42)
    .AddField<0, int>(42)
    .AddField<0, int>(42)
    .AddField<0, int>(42)
    .AddField<0, int>(42)
    .AddField<0, int>(42)
    .AddField<0, int>(42)
    // .AddField<0, int>(42) compilation failure is here
    ;
  static ::ninrob::trace::TracePointId tp =
    ::ninrob::trace::GetNextTracePointId();
  res.Publish(tp, "love me or leave me");
  (void)res;
}


TEST( Trace, ProcessWideRegistry) {
  {
    auto& registry = ::ninrob::trace::GetProcessWideSchemeGeneratorsRegistry();
    ASSERT_EQ(registry.size(), 2U);
  }

  {
    auto& registry = ::ninrob::trace::GetInternalSchemeRegistry();
    ASSERT_EQ(registry.size(), 2U);
  }
}


TEST( Log, Simple) {
  ::ninrob::trace::Fatal([](::ninrob::trace::TracePointId const tp) noexcept {
    ::std::cerr << "yes, I did it with trace id: " << tp << ::std::endl;
  });
}
