#pragma once
#include "compiler.hh"
#include <type_traits>
#include <cstdarg>
#include <utility>

namespace ninrob {


NINROB_NO_RETURN
NINROB_COMPILE_COLD_CODE
void AbortF( const  char* errmsg, ...) noexcept;


NINROB_NO_RETURN
NINROB_COMPILE_COLD_CODE
void AbortVF( const char* errmsg, ::std::va_list arglist) noexcept;


template <class ValueType>
static inline
NINROB_ARTIFICIAL_FUNCTION
::std::conditional_t<
  ::std::is_lvalue_reference_v<ValueType>,
  ValueType,
  typename ::std::decay_t<ValueType>>
VerifyF( ValueType&& value, const char* errmsg = nullptr, ...) noexcept {
  if (NINROB_SURE(value)) {
    return ::std::forward<ValueType>( value);
  }

  ::std::va_list arglist;
  va_start( arglist, errmsg);
  AbortVF( errmsg, arglist);
}


}
