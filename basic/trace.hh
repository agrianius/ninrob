#pragma once
#include <array>
#include <atomic>
#include <cstddef>
#include <type_traits>

#include "int_types.hh"
#include "compiler.hh"
#include "log_severity.hh"

namespace ninrob::trace {


static constexpr u3 MAX_NUMBER_OF_FIELDS = 32;
static constexpr u4 MAX_FORMAT_MESSAGE_SIZE = 4095;


using FieldId = u5;
using FormatMessageId = u5;


struct RegistrationParameters {
  TracePointId scheme_id = 0;
  FormatMessageId format_message_id = 0;
  const FieldId* begin_ids = nullptr;
  const FieldId* end_ids = nullptr;
  const u4* begin_offsets = nullptr;
  const u4* end_offsets = nullptr;
  const u3* begin_field_type_ids = nullptr;
  const u3* end_field_type_ids = nullptr;
};


struct FieldIdContainer {
  virtual void FillIds( const RegistrationParameters& params) noexcept = 0;
};


using SchemeGenerator = void(*)( FieldIdContainer* container) noexcept;


void AddSchemeGeneratorToProcessWideRegistry( SchemeGenerator) noexcept;


enum class FieldTypeEnum: u3 {
  NULLSTR, // const char*
  U3,  // uint8
  S3,  // int8
  U4,  // uint16
  S4,  // int16
  U5,  // uint32
  S5,  // int32
  U6,  // uint64
  S6,  // int64
  FL5,  // float
  FL6,  // double

  MAX,  // max limit of FieldType
};


template <class T>
struct FieldTypeEnumChoice;


template <>
struct FieldTypeEnumChoice<const char*> {
  static constexpr FieldTypeEnum field_type = FieldTypeEnum::NULLSTR;
  static constexpr u3 field_value_size = sizeof(const char*);
  static constexpr u3 required_buffer_size = 0;
};


template <>
struct FieldTypeEnumChoice <u3> {
  static constexpr FieldTypeEnum field_type = FieldTypeEnum::U3;
  static constexpr u3 field_value_size = sizeof(u3);
  static constexpr u3 required_buffer_size = 3;  // 255
};


template <>
struct FieldTypeEnumChoice <s3> {
  static constexpr FieldTypeEnum field_type = FieldTypeEnum::S3;
  static constexpr u3 field_value_size = sizeof(s3);
  static constexpr u3 required_buffer_size = 4;  // -128
};


template <>
struct FieldTypeEnumChoice <u4> {
  static constexpr FieldTypeEnum field_type = FieldTypeEnum::U4;
  static constexpr u3 field_value_size = sizeof(u4);
  static constexpr u3 required_buffer_size = 5;  // 65535
};


template <>
struct FieldTypeEnumChoice <s4> {
  static constexpr FieldTypeEnum field_type = FieldTypeEnum::S4;
  static constexpr u3 field_value_size = sizeof(s4);
  static constexpr u3 required_buffer_size = 6;  // -32768
};


template <>
struct FieldTypeEnumChoice <u5> {
  static constexpr FieldTypeEnum field_type = FieldTypeEnum::U5;
  static constexpr u3 field_value_size = sizeof(u5);
  static constexpr u3 required_buffer_size = 10;  // 4294967295
};


template <>
struct FieldTypeEnumChoice <s5> {
  static constexpr FieldTypeEnum field_type = FieldTypeEnum::S5;
  static constexpr u3 field_value_size = sizeof(s5);
  static constexpr u3 required_buffer_size = 11;  // -2147483648
};


template <>
struct FieldTypeEnumChoice <u6> {
  static constexpr FieldTypeEnum field_type = FieldTypeEnum::U6;
  static constexpr u3 field_value_size = sizeof(u6);
  static constexpr u3 required_buffer_size = 20;  // 1844674407370955165
};


template <>
struct FieldTypeEnumChoice <s6> {
  static constexpr FieldTypeEnum field_type = FieldTypeEnum::S6;
  static constexpr u3 field_value_size = sizeof(s6);
  static constexpr u3 required_buffer_size = 20;  // -9223372036854775808
};


template <>
struct FieldTypeEnumChoice <float> {
  static constexpr FieldTypeEnum field_type = FieldTypeEnum::FL5;
  static constexpr u3 field_value_size = sizeof(float);
  static constexpr u3 required_buffer_size = 16;  // https://stackoverflow.com/a/52045523
};


template <>
struct FieldTypeEnumChoice <double> {
  static constexpr FieldTypeEnum field_type = FieldTypeEnum::FL6;
  static constexpr u3 field_value_size = sizeof(double);
  static constexpr u3 required_buffer_size = 24;  // https://stackoverflow.com/a/52045523
};


template <class BaseType = void>
struct NamedTupleConstructor {
  template <FieldId p_id, class FieldType>
  constexpr auto AddField( FieldType value) noexcept;

  template <u3 N>
  constexpr auto Get() const noexcept {
    static_assert(N <= BaseType::GetIndex());
    return Get<BaseType::GetIndex() - N>(
        static_cast<const BaseType*>(this)->payload);
  }

  template <u3 N, class Payload>
  static constexpr auto Get( Payload payload) noexcept {
    if constexpr (N == 0) {
      return payload.val;
    } else {
      return Get<N - 1>( payload.before);
    }
  }

  template <u3 N>
  static constexpr u4 GetOffset() noexcept {
    static_assert(N <= BaseType::GetIndex());
    return GetOffsetImpl<
      BaseType::GetIndex() - N,
      typename BaseType::Payload>();
  }

  template <u3 N, class Payload>
  static constexpr u4 GetOffsetImpl() noexcept {
    if constexpr (N == 0) {
      return offsetof( Payload, val);
    } else {
      return GetOffsetImpl<N - 1, decltype( Payload::before)>();
    }
  }

  void Publish(TracePointId const trace_point_id, const char* msg) noexcept;
};


template <class BaseType>
template <FieldId p_id, class FieldType>
constexpr auto
NamedTupleConstructor<BaseType>::AddField( FieldType value) noexcept {
  if constexpr (::std::is_same_v<BaseType, void>) {
    struct Enriched: public NamedTupleConstructor<Enriched> {
      explicit Enriched( FieldType _val) noexcept
        : payload{ .val = _val}
      {}

      struct Payload {
        FieldType val;
      };

      Payload payload;

      static constexpr ::std::array<FieldId, 1> GetIds() noexcept {
        return {p_id};
      }

      static constexpr u3 GetIndex() noexcept {
        return 0;
      };

      static constexpr ::std::array<u4, 1> GetOffsets() noexcept {
        constexpr auto offset = offsetof(Payload, val);
        static_assert(offset == 0);
        return {offset};
      }

      static constexpr ::std::array<u3, 1>
      GetFieldTypeIds() noexcept {
        return {static_cast<u3>(FieldTypeEnumChoice <FieldType>::field_type)};
      }
    };

    return Enriched{ value};
  } else {
    struct Enriched: public NamedTupleConstructor<Enriched> {
      Enriched(typename BaseType::Payload _before, FieldType _val) noexcept
        : payload{
            .before = _before,
            .val = _val,
          }
      {}

      struct Payload {
        typename BaseType::Payload before;
        FieldType val;
      };

      Payload payload;

      static constexpr auto GetIds() noexcept {
        auto base_ids = BaseType::GetIds();
        ::std::array< FieldId, base_ids.size() + 1> result;
        ::std::copy( base_ids.begin(), base_ids.end(), result.begin());
        result.back() = p_id;
        return result;
      }

      static constexpr u3 GetIndex() noexcept {
        static_assert(
            BaseType::GetIndex() + 1 < MAX_NUMBER_OF_FIELDS,
            "too many fields");
        return BaseType::GetIndex() + 1;
      }

      static constexpr auto GetOffsets() noexcept {
        auto base_offsets = BaseType::GetOffsets();
        ::std::array< u4, base_offsets.size() + 1> result;
        ::std::copy( base_offsets.begin(), base_offsets.end(), result.begin());
        result.back() = offsetof(Payload, val);
        return result;
      }

      static constexpr auto GetFieldTypeIds() noexcept {
        auto base_ids = BaseType::GetFieldTypeIds();
        ::std::array< u3, base_ids.size() + 1> result;
        ::std::copy( base_ids.begin(), base_ids.end(), result.begin());
        result.back() =
          static_cast<u3>(FieldTypeEnumChoice <FieldType>::field_type);
        return result;
      }
    };

    auto base_self = static_cast<BaseType*>(this);
    return Enriched{ base_self->payload, value};
  }
};


struct InternalLogRecord {
  const char* format;
  const char* data;
  TracePointId scheme_id;
  TracePointId trace_point_id;
};


struct Publisher {
  virtual void PublishLogRecord(InternalLogRecord rec) noexcept = 0;
};


extern ::std::atomic<Publisher*> process_wide_publisher;
extern ::std::atomic<TracePointId> process_wide_scheme_id_source;


template <class BaseType>
struct SchemeInstantiator {
  NINROB_CALL_BEFORE_MAIN_WITH_PRIORITY(101)
  NINROB_COMPILE_COLD_CODE
  static void RegisterScheme() noexcept {
    AddSchemeGeneratorToProcessWideRegistry(
        &SchemeInstantiator::SchemeGenerator);
  }

  NINROB_COMPILE_COLD_CODE
  static void SchemeGenerator( FieldIdContainer* container) noexcept {
    auto ids = BaseType::GetIds();
    auto offsets = BaseType::GetOffsets();
    auto field_type_ids = BaseType::GetFieldTypeIds();
    RegistrationParameters params{
      .scheme_id = GetSchemeId(),
      .format_message_id = 42,
      .begin_ids = ids.cbegin(),
      .end_ids = ids.cend(),
      .begin_offsets = offsets.cbegin(),
      .end_offsets = offsets.cend(),
      .begin_field_type_ids = field_type_ids.cbegin(),
      .end_field_type_ids = field_type_ids.cend(),
    };
    container->FillIds(params);
  }

  NINROB_COMPILE_COLD_CODE
  static TracePointId GetSchemeId() noexcept {
    static TracePointId scheme_id = process_wide_scheme_id_source.fetch_add(
        1, ::std::memory_order_relaxed) + 1;
    return scheme_id;
  }
};


template <class BaseType>
void NamedTupleConstructor<BaseType>::Publish(
    TracePointId const trace_point_id, const char* msg) noexcept
{
  // instantiate registrator
  (void)&SchemeInstantiator<BaseType>::RegisterScheme;

  auto publisher = process_wide_publisher.load(::std::memory_order_relaxed);
  if (NINROB_DOUBT(publisher == nullptr)) {
    return;
  }
  InternalLogRecord rec{
    .format = msg,
    .data = reinterpret_cast<const char*>(
        &static_cast<const BaseType*>(this)->payload),
    .scheme_id = SchemeInstantiator<BaseType>::GetSchemeId(),
    .trace_point_id = trace_point_id,
  };
  publisher->PublishLogRecord(rec);
}


}
