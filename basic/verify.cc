#include "verify.hh"
#include <cstdarg>
#include <cstdio>
#include <cstdlib>

namespace ninrob {


NINROB_NO_RETURN
NINROB_COMPILE_COLD_CODE
void AbortF( const char* format, ...) noexcept {
  if (format != nullptr) {
    std::va_list arglist;
    va_start( arglist, format);
    std::vfprintf( stderr, format, arglist);
  }
  std::abort();
}


NINROB_NO_RETURN
NINROB_COMPILE_COLD_CODE
void AbortVF( const char* format, std::va_list arglist) noexcept {
  if (format != nullptr) {
    std::vfprintf( stderr, format, arglist);
  }
  std::abort();
}


}
