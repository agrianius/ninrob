#pragma once
#include "compiler.hh"
#include "int_types.hh"
#include <atomic>
#include <utility>

namespace ninrob::trace {


enum class Severity: u3 {
  OFF,
  FATAL,
  ALERT,
  ERROR,
  WARN,
  BRIEF,
  INFO,
  VERBOSE,
  DEBUG,
  MAX_VALUE,
};


using TracePointId = u5;


extern ::std::atomic< Severity> process_wide_opted_severity;
extern ::std::atomic< TracePointId> process_wide_tracepoint_id_source;


static inline TracePointId GetNextTracePointId() noexcept {
  return process_wide_tracepoint_id_source.fetch_add(
      1, ::std::memory_order_relaxed) + 1;
}


template <class>
struct TracePointForCallable {
  static bool IsActive() noexcept {
    return GetTracepointId() != 0;
  }

  static u5 GetTracepointId() noexcept {
    static u5 tracepoint_id = GetNextTracePointId();
    return tracepoint_id;
  }
};


template <class Callable>
void Log(Severity severity, Callable&& conditional) noexcept {
  Severity opted_severity =
    process_wide_opted_severity.load(::std::memory_order_relaxed);
  if (opted_severity < severity &&
      !TracePointForCallable<Callable>::IsActive())
  {
    // omit the log point
    return;
  }
  conditional(TracePointForCallable<Callable>::GetTracepointId());
}



template <class Callable>
void Fatal(Callable&& conditional_callable) noexcept {
  Log(Severity::FATAL, ::std::forward<Callable>(conditional_callable));
}


template <class Callable>
void Alert(Callable&& conditional_callable) noexcept {
  Log(Severity::ALERT, ::std::forward<Callable>(conditional_callable));
}


template <class Callable>
void Error(Callable&& conditional_callable) noexcept {
  Log(Severity::ERROR, ::std::forward<Callable>(conditional_callable));
}


template <class Callable>
void Warn(Callable&& conditional_callable) noexcept {
  Log(Severity::WARN, ::std::forward<Callable>(conditional_callable));
}


template <class Callable>
void Brief(Callable&& conditional_callable) noexcept {
  Log(Severity::BRIEF, ::std::forward<Callable>(conditional_callable));
}


template <class Callable>
void Info(Callable&& conditional_callable) noexcept {
  Log(Severity::INFO, ::std::forward<Callable>(conditional_callable));
}


template <class Callable>
void Verbose(Callable&& conditional_callable) noexcept {
  Log(Severity::VERBOSE, ::std::forward<Callable>(conditional_callable));
}


template <class Callable>
void Debug(Callable&& conditional_callable) noexcept {
  Log(Severity::DEBUG, ::std::forward<Callable>(conditional_callable));
}


}
